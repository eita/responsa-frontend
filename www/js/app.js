// Responsa - App do Portal do Consumo Repsonsável

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('responsa', ['ionic', 'ngResource',
               'controllers', 'directives', 'resources', 'factories', 'filters',
               'ng-token-auth',"leaflet-directive",'ngCordova','ngCachedResource'])

  .run(function($ionicPlatform, $rootScope, ResponsaAuth, $window, $state, $ionicHistory, $ionicSideMenuDelegate) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      //when false, it will fetch when entering the page "favoritos"
      $rootScope.favoritos_updated = false;

      function onResponsaHardwareBackButton(e) {
        var backView = $ionicHistory.backView();
        if (backView) {
          // there is a back view, go to it
          backView.go();
        } else {
          // there is no back view
          var currentHistory = $ionicHistory.viewHistory();

          //If 'encontros' view: close the app instead
          if (currentHistory.currentView.stateName == 'app.encontre') {
            ionic.Platform.exitApp();
          } else {
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            $ionicHistory.goBack();
          }

        }
        e.preventDefault();
        return false;
      }
      $ionicPlatform.registerBackButtonAction(
          onResponsaHardwareBackButton,
          100 //PLATFORM_BACK_BUTTON_PRIORITY_VIEW
      );


      //Workaround: close sidemenu with swipe
      // WORKAROUND FOR IONIC BUG ON ANDROID
      document.addEventListener('touchstart', function (event) {
        if ($ionicSideMenuDelegate.isOpenLeft()) {
          event.preventDefault();
        }
      });


      document.addEventListener("backbutton", function() {
        $window.history.back();
        console.log('back button');
      }, false);

      ResponsaAuth.validateUser();
    });
  })


  .config(function($stateProvider, $urlRouterProvider, $authProvider, $rootScopeProvider, $ionicConfigProvider) {

      //Native scroll for android
      // See http://forum.ionicframework.com/t/native-scrolling-android-testers-wanted/17059
    if (!ionic.Platform.isIOS()) {
      $ionicConfigProvider.scrolling.jsScrolling(false);
    }

    $stateProvider

    .state('login', {
          url: '/login',
          templateUrl: 'templates/login.html',
          controller: 'LoginController',

          resolve: {
            auth: function($auth, $rootScope) {
              return $rootScope.cachedUser == null;
              //return $auth.userIsAuthenticated() == null;
            }
          }
        })

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'ResponsaController',

      resolve: {
        auth: function($auth, $rootScope) {
          return $rootScope.cachedUser != null;
          //return $auth.validateUser();
        }
      }
    })

    .state('app.user_config', {
      url: '/app/user_config',
      views: {
        'mainView' : {
          templateUrl: 'templates/user_config.html',
          controller: 'UserConfigController'
        }
      }
    })

    .state('app.encontre', {
      url: "/encontre",
      views: {
        'mainView': {
          templateUrl: "templates/encontre/main.html",
          controller: 'EncontreController'
        }
      }
    })

    .state('app.ficha_sintese', {
      url: "/ficha_sintese/:id",
      views: {
        'mainView': {
          templateUrl: "templates/ficha_sintese.html",
          controller: 'FichaSinteseController'
        }
      }
    })

    .state('app.cheguei', {
      url: "/cheguei",
      views: {
        'mainView': {
          templateUrl: "templates/user_actions/cheguei-escolher.html",
          controller: 'ChegueiController'
        }
      }
    })

    .state('app.boicotes',{
          url: '/boicotes',
          views: {
            'mainView' : {
              templateUrl: 'templates/boicotes/index.html',
              controller: 'BoicotesController'
            }
          }
        })

    .state('app.boicote',{
          url: '/boicotes/:id',
          views: {
            'mainView': {
              templateUrl: 'templates/boicotes/show.html',
              controller: 'BoicoteController'
            }
          }
        })


  // Testes

    .state('app.tests', {
      url: "/tests",
      views: {
        'mainView': {
          templateUrl: "templates/tests/index.html",
          controller: 'MainTestController'
        }
      }
    })

        .state('app.slide_tests', {
          url: '/tests/slide',
          views: {
            'mainView' : {
              templateUrl: 'templates/tests/slide.html'
            }
          }
        })

    .state('app.camera_tests', {
      url: "/tests/camera",
      views: {
        'mainView': {
          templateUrl: "templates/tests/camera.html",
          controller: 'CameraTestController'
        }
      }
    })

    .state('app.geolocation_tests', {
      url: "/tests/geolocation",
      views: {
        'mainView': {
          templateUrl: "templates/tests/geolocation.html",
          controller: 'GeolocationTestController'
        }
      }
    });

    // if none of the above states are matched, use this as the fallback
    //$urlRouterProvider.otherwise('/login');

    $authProvider.configure({
      apiUrl: responsa.settings.apiUrl,
      storage: 'localStorage',
      validateOnPageLoad: false //validates manually

    });

  });








