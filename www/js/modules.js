//INIT MODULES

angular.module('resources', []);
angular.module('factories', []);
angular.module('controllers', ['resources', 'angularFileUpload']);
angular.module('filters', []);
angular.module('directives', []);

