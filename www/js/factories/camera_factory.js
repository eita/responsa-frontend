angular.module('factories')

.factory('CameraFactory', ['$q', function($q) {

  return {
    getPicture: function(options) {
      var q = $q.defer();

      var cameraSuccess = function(result) {
        // Do any magic you need
        q.resolve(result);
      };
      var cameraError = function(err) {
        q.reject(err);
      };

      navigator.camera.getPicture(cameraSuccess, cameraError, options);

      return q.promise;
    }
  }
}]);
