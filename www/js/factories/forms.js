angular.module('factories')
    .factory('Forms', function(ResponsaObject, $ionicModal, $state, Location, $rootScope) {
       return {
           local: {
               new: function(scope)  {
                    ro = new ResponsaObject({type: 'Local'});
                    return this.edit(ro,scope);
               },

               edit: function(local,parentScope) {
                   scope = parentScope.$new();
                   scope.ro = local;
                    $ionicModal.fromTemplateUrl('templates/forms/local.html',{
                        scope: scope,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        scope.localEditModal = modal;
                        modal.show();
                    });
                   scope.openModal = function() {
                       scope.localEditModal.show();
                   };
                   scope.closeModal = function() {
                       scope.localEditModal.hide();
                   };
                   //Cleanup the modal when we're done with it!
                   scope.$on('$destroy', function() {
                       scope.localEditModal.remove();
                   });

                   scope.save = function() {

                       local.lat = $rootScope.bgCurrentPosition.latitude;
                       local.lng = $rootScope.bgCurrentPosition.longitude;

                       scope.ro.$save(function(local, putResponseHeaders) {
                           $state.go('app.ficha_sintese',{id: local.id, obj: local});
                           scope.closeModal();
                       })
                   }

               }


           },
           encontro: {
               new: function(scope)  {
                   ro = new ResponsaObject({type: 'Encontro'});
                   return this.edit(ro,scope);
               },

               edit: function(encontro,parentScope) {
                   scope = parentScope.$new();
                   scope.ro = encontro;
                   $ionicModal.fromTemplateUrl('templates/forms/encontro.html',{
                       scope: scope,
                       animation: 'slide-in-up'
                   }).then(function(modal) {
                       scope.encontroEditModal = modal;
                       modal.show();
                   });
                   scope.openModal = function() {
                       scope.encontroEditModal.show();
                   };
                   scope.closeModal = function() {
                       scope.encontroEditModal.hide();
                   };
                   //Cleanup the modal when we're done with it!
                   scope.$on('$destroy', function() {
                       scope.encontroEditModal.remove();
                   });

                   scope.save = function() {
                       scope.ro.$save(function(encontro, putResponseHeaders) {
                           $state.go('app.ficha_sintese',{id: encontro.id, obj: encontro});
                           scope.closeModal();
                       })
                   };

               }
           },
           responsa_object: {
               search: function(parentScope) {
                   scope = parentScope.$new();
                   scope.responsa_objects = [];
                   scope.searching = false;
                   $ionicModal.fromTemplateUrl('templates/encontre/pesquisar.html',{
                       scope: scope,
                       animation: 'slide-in-up'
                   }).then(function(modal) {
                       scope.roSearchModal = modal;
                       modal.show();
                   });
                   scope.openModal = function() {
                       scope.roSearchModal.show();
                   };
                   scope.closeModal = function() {
                       scope.roSearchModal.hide();
                   };
                   //Cleanup the modal when we're done with it!
                   scope.$on('$destroy', function() {
                       scope.roSearchModal.remove();
                   });

                   scope.search = function(keyword) {
                        if (!keyword || keyword.length < 2) {
                            scope.responsa_objects = [];
                        }
                        else {
                            scope.responsa_objects = ResponsaObject.query({palavra_chave : keyword}, function(results) {
                                scope.searching = false;
                                scope.results_arrived = true;
                            });
                            scope.searching = true;
                        }
                   };
                   parentScope.handleClickResultado = function(responsa_object_id) {
                       $state.go('app.ficha_sintese', {id: responsa_object_id});
                       scope.closeModal() ;
                       setTimeout(function() {scope.$destroy()},400);
                   };

               }
           },
           user_action: {
               cheguei: function(parentScope) {
                   scope = parentScope.$new();
                   scope.responsa_objects = [];
                   scope.searching = false;
                   $ionicModal.fromTemplateUrl('templates/user_action/cheguei.html',{
                       scope: scope,
                       animation: 'slide-in-up'
                   }).then(function(modal) {
                       scope.roSearchModal = modal;
                       modal.show();
                   });
                   scope.openModal = function() {
                       scope.roSearchModal.show();
                   };
                   scope.closeModal = function() {
                       scope.roSearchModal.hide();
                   };
                   //Cleanup the modal when we're done with it!
                   scope.$on('$destroy', function() {
                       scope.roSearchModal.remove();
                   });

                   scope.search = function(keyword) {
                       if (!keyword || keyword.length < 2) {
                           scope.responsa_objects = [];
                       }
                       else {
                           scope.responsa_objects = ResponsaObject.query({palavra_chave : keyword}, function(results) {
                               scope.searching = false;
                               scope.results_arrived = true;
                           });
                           scope.searching = true;
                       }
                   };

                   scope.$on('ro_result_selected',function() {
                       scope.closeModal() ;
                       setTimeout(function() {scope.$destroy()},400);
                   });
               }
           }
       }
    });