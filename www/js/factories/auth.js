(function() {

    angular.module('factories')
        .factory('ResponsaAuth', function ($rootScope, $state, $auth, $window) {

            //Status of the connection to the authentication server
            $rootScope.authOnline = null;

            //ng-token-auth event handling
            $rootScope.$on('auth:login-success', loginSuccess);
            $rootScope.$on('auth:login-error', loginError);
            $rootScope.$on('auth:validation-success', validationSuccess);
            $rootScope.$on('auth:validation-error', validationError);
            $rootScope.$on('auth:invalid', authInvalid);
            $rootScope.$on('auth:logout-success', logoutSuccess);

            return {
                validateUser: validateUser,
                online: online
            };

            function online() {
                return $rootScope.authOnline;
            }

            function validateUser() {
                loadCachedUser();
                //After all event listeners set, validate user
                $auth.validateUser();
                return $rootScope.cachedUser;
            }

            function loginSuccess(ev, user) {
                console.log('auth:login-success');
                setCachedUser(user);
                $state.transitionTo('app.encontre');
            }

            function loginError(ev, reason) {
                console.log('auth:login-error');
                setCachedUser(null);
                $state.transitionTo('login');
            }

            function validationSuccess(ev, user) {
                console.log('auth:validation-success');
                $rootScope.authOnline = true;
            }

            function validationError(ev, user) {
                console.log('auth:validation-error');
                $rootScope.authOnline = false;
            }

            function authInvalid(ev) {
                //Deve tentar fazer login novamente com a senha guardada antes de deslogar
                console.log('auth:invalid');
                setCachedUser(null);
                $state.transitionTo('login');
            }

            function logoutSuccess(ev) {
                console.log('auth:invalid');
                setCachedUser(null);
                $state.transitionTo('login');
            }

            function setCachedUser(user) {
                $rootScope.cachedUser = user;
                $window.localStorage.setItem('responsa.cached_user',JSON.stringify(user));
            }

            function loadCachedUser() {
                if ($rootScope.cachedUser == null) {
                    try {
                        $rootScope.cachedUser = JSON.parse($window.localStorage.getItem('responsa.cached_user'));
                        $rootScope.authOnline = false;
                        var current_state_name = $state.$current.name;
                        if ($rootScope.cachedUser != null && current_state_name == "" || current_state_name == 'login') {
                            $state.transitionTo('app.encontre')
                        }
                    } catch (e) {
                        $rootScope.cachedUser = null;
                        $rootScope.authOnline = null;
                    }
                }
            }
        });

})();
