angular.module('factories')
    .factory('Location', function($rootScope, $http, $q) {
       return {
           guessAddress: function() {
               var q = $q.defer();
               var url = window.responsa.settings.apiUrl + '/guess_address';
               $http.get(url,$rootScope.currentPosition).success(function (data, status) {
                   q.resolve(data);
               }).error(function(data, status) {
                   q.reject(data);
                   //TODO Mostrar erro?
               });
               return q.promise;
           }
       };
    })

    .factory('BackgroundLocation',function($rootScope, $window, rDebug) {
        return {

            lastPositions: [],

            registerPosition: function(location) {
                var distance;
                var self = this;
                var currentTime = (new Date()).getTime();
                var maxAllowedTime = $rootScope.settings.initiativesUpdateInterval * 60;


                //register current space-time into lastPositions
                self.lastPositions.push({location: location, time: currentTime});
                rDebug.log('bgLocation registered position. Stack: ' + JSON.stringify(self.lastPositions));


                //cleans all positions array where position is far or time too old
                var deleteAllBefore = false;
                for (var i =  self.lastPositions.length; i>=0; --i) {
                    if (deleteAllBefore) {
                        self.lastPositions.splice(i,1);
                    } else {
                        //if time is too old, discard this and others before.
                        if (self.lastPositions[i].time < currentTime - maxAllowedTime && !(self.lastPositions[i+1] && self.lastPositions[i+1].time >= currentTime - maxAllowedTime)) {
                            self.lastPositions.splice(i,1);
                            deleteAllBefore = true;
                            continue;
                        }

                        //if distance is too far away, discard this and others before
                        distance = geolib.getDistance(location, self.lastPositions[i].location, 10);
                        if (distance > $rootScope.settings.metersToConsiderInPlace) {
                            self.lastPositions.splice(i,1);
                            deleteAllBefore = true;
                        }
                    }
                }
                rDebug.log('bgLocation deleted some. Stack: '+JSON.stringify(self.lastPositions));

                //user is stopped in the same place for a while
                var stoppedInPlace = (self.lastPositions[0] && self.lastPositions[0].time < currentTime - maxAllowedTime);

                //user had not received the last notification from this place
                var lastNotificationNotFromThisPlace = (!$rootScope.lastStoppedNotificationPosition || geolib.getDistance(location, $rootScope.lastStoppedNotificationPosition, 10) > $rootScope.settings.metersToConsiderInPlace);

                //user had not received notifications recently
                var notReceivedNotificationsRecently = ($rootScope.lastStoppedNotificationTime < currentTime - maxAllowedTime);

                rDebug.log('bgLocation stoppedInPlace: '+stoppedInPlace);
                rDebug.log('bgLocation lastNotificationNotFromThisPlace' + lastNotificationNotFromThisPlace);
                rDebug.log('bgLocation notReceivedNotificationsRecently'+ notReceivedNotificationsRecently);

                if (stoppedInPlace && lastNotificationNotFromThisPlace && notReceivedNotificationsRecently) {
                    $rootScope.lastStoppedNotificationTime = currentTime;
                    $rootScope.lastStoppedNotificationPosition = location;
                    $rootScope.$broadcast('stoppedInPlace');
                    rDebug.log('bgLocation broadcast:stoppedInPlace');
                    alert('stopped in place');
                }
            },

            startMonitor: function() {
                var self = this;
                //only works if it is running in mobile
                if ($window.plugins) {
                    var bgGeo = $window.plugins.backgroundGeoLocation;

                    var bgGeoError = function() {
                        console.log('bgGeo error');
                    };

                    var bgGeoNotify = function(location) {
                        if (responsa.settings.debug) {
                            console.log('bgGeo: '+JSON.stringify(location));
                        }
                        if ($rootScope) {
                            self.registerPosition(location);
                        }
                        bgGeo.finish();
                    };

                    var bgGeoOptions = {
                        distanceFilter: 1,
                        desiredAccuracy: 0,
                        stationaryRadius: 1,
                        debug: false
                    };

                    bgGeo.configure(bgGeoNotify, bgGeoError, bgGeoOptions);
                    bgGeo.start();
                }
            }

        }
    })
;