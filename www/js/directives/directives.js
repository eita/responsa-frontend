angular.module('directives')
.directive('editButton', function() {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/edit-button.html'
  }
})

.directive('pageSpinner',function() {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/page-spinner.html'
  }
    });

