angular.module('resources')

    .factory('ResponsaObject',function($cachedResource,$http, Comentario, $q, $cordovaSocialSharing, Location, $rootScope){
        var RO = $cachedResource('responsa_object',window.responsa.settings.apiUrl + '/responsa_objects/:id.json',{id:'@id'},{update:{method:'PUT'}});

        RO.prototype.classType = 'ResponsaObject';

        RO.prototype.comentarios = null;

        RO.prototype.marker = function() {
            if (this.lat && this.lng) {
                return {
                    lat: this.lat,
                    lng: this.lng,
                    message: '<a href="#/app/ficha_sintese/' + this.id + '">' + this.title + '</a>'
                };
            } else {
                return null;
            }
/*
            if (this.lat && this.lng) {
                return L.marker([this.lat,this.lng]);
            }
            else {
                return null;
            }
                    //.bindPopup('<a href="#/app/ficha_sintese/'+this.id+'">'+this.title+'</a>')
                    */

        };

        RO.prototype.gostar = function() {
            var url = window.responsa.settings.apiUrl + '/responsa_objects/gostar/'+this.id+'.json';
            var self = this;
            self.gostou = true; //show immediatelly
            $http.get(url).success(function (data, status) {
                self.gostou = true;
            }).error(function(data, status) {
                self.gostou = false;
                //TODO Mostrar erro?
            });
        };

        RO.prototype.desgostar = function() {
            var url = window.responsa.settings.apiUrl + '/responsa_objects/desgostar/'+this.id+'.json';
            var self = this;
            self.gostou = false; //show immediatelly
            $http.get(url).success(function (data, status) {
                self.gostou = false;
            }).error(function(data, status) {
                //TODO Mostrar erro?
            });
        };

        RO.prototype.favoritar = function() {
            var url = window.responsa.settings.apiUrl + '/responsa_objects/favoritar/'+this.id+'.json';
            var self = this;
            $http.get(url).success(function (data, status) {
                self.favoritou = true;
                $rootScope.favoritos_updated = false;
            }).error(function(data, status) {
                self.favoritou = false;
                //TODO Mostrar erro?
            });
        };

        RO.prototype.desfavoritar = function() {
            var url = window.responsa.settings.apiUrl + '/responsa_objects/desfavoritar/'+this.id+'.json';
            var self = this;
            $http.get(url).success(function (data, status) {
                self.favoritou = false;
                $rootScope.favoritos_updated = false;
            }).error(function(data, status) {
                //TODO Mostrar erro?
            });
        };

        RO.prototype.marcar_presenca = function() {
            var url = window.responsa.settings.apiUrl + '/responsa_objects/marcar_presenca/'+this.id+'.json';
            var self = this;
            $http.get(url).success(function (data, status) {
                self.marcou_presenca = true;
            }).error(function(data, status) {
                self.marcou_presenca = false;
                //TODO Mostrar erro?
            });
        };

        RO.prototype.desmarcar_presenca = function() {
            var url = window.responsa.settings.apiUrl + '/responsa_objects/desmarcar_presenca/'+this.id+'.json';
            var self = this;
            $http.get(url).success(function (data, status) {
                self.marcou_presenca = false;
            }).error(function(data, status) {
                //TODO Mostrar erro?
            });
        };


        RO.prototype.toggleGostar = function() {
            if (this.gostou) {
                this.desgostar();
            } else {
                this.gostar();
            }
        };

        RO.prototype.toggleFavoritar = function() {
            if (this.favoritou) {
                this.desfavoritar();
            } else {
                this.favoritar();
            }
        };

        RO.prototype.toggleMarcarPresenca = function() {
            if (this.favoritou) {
                this.desmarcar_presenca();
            } else {
                this.marcar_presenca();
            }
        };

        RO.prototype.compartilhar = function() {
            alert('compartilhei!')

        };

        RO.prototype.loadComentarios = function() {
            var self = this;
            var deferred = $q.defer();

            Comentario.query({comentavel_id: this.id, comentavel_type: 'ResponsaObject'}, function(comentarios) {
                self.comentarios = comentarios;
                deferred.resolve(comentarios);
            },function() {
                deferred.reject({status: 'error', msg: 'Não pôde carregar comentários'})
            });

            return deferred.promise;
        };

        RO.prototype.novoComentario = function() {

        };

        RO.prototype.compartilhar = function() {
            message = this.title + ': ' + this.address_line1 + "\n" + this.phone_number1;
            subject = this.title + ': iniciativa do consumo responsável';
            file = null;
            link = null;
            $cordovaSocialSharing
                .share(message, subject, file, link) // Share via native share sheet
                .then(function(result) {
                    //alert('success!'); //Ganha pontos
                }, function(err) {
                // An error occured. Show a message to the user
            });
        };


        RO.prototype.guessAddress = function() {
            var self = this;
            alert('a');
            var fillAddress = function(address) {
                self.address_line1 = address;
            };
            Location.guessAddress().then(fillAddress)
        };

        RO.prototype.viewInNativeMap = function() {
            if (device) {
                switch (device.platform) {
                    case 'Android':
                        window.open('geo:'+this.lat+','+this.lng);
                    case 'iOS':
                        window.open('maps:'+this.lat+','+this.lng); //TODO test
                }

            } else { //Browser testing
                window.open('geo:'+this.lat+','+this.lng);
            }
        };

        return RO;
    });
