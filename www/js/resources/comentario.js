angular.module('resources')

    //Não funcionou adicionar um novo comentario a uma lista de comentarios, com o CachedResource
    .factory('Comentario', function($resource) {
        return $resource(window.responsa.settings.apiUrl + '/comentarios/:id.json',{id:'@id'},{update:{method:'PUT'}});
    });

/*
    Comentavel:
    Precisa ter o método loadComentarios()

 */
