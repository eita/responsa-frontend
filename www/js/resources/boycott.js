angular.module('resources')

    .factory('Boycott',function($cachedResource) {
        var BO = $cachedResource('boycott',window.responsa.settings.apiUrl + '/boycotts/:id.json',{id:'@id'},{update:{method:'PUT'}});

        BO.prototype.classType = 'Boycott';

        return BO;
    });

