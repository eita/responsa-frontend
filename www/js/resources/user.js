angular.module('resources')

.factory('User', function($cachedResource) {
  var user = $cachedResource('user',
                             responsa.settings.apiUrl + '/users/:id.json', {id:'@id'},
                             {update: {method:'PUT'}});
  user.prototype.login_or_name = function() {
    if (this.nome) {
      return this.nome;
    } else if (this.email) {
      return this.email;
    }
  };

  return user;
});

