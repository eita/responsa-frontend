responsa.settings = {
  apiUrl: 'http://localhost:3000',

  testLocation: false,
  //testLocation: {latitude: -22.9873355,longitude:-43.4608085},

  //testLocationTimeout: 3000, //timeout for test location to resolve, to emulate spinnings/loading indicators


  // enable debug on plugins, alerts and logs
  debug: true,
  // 1) install: sudo npm install -g weinre
  // 2) run: weinre --boundHost -all-
  // 3) open on browser: http://localhost:8080/client/#anonymous
  // 4) watch targets appear as you open the app.
  // enjoy!
  debugUrl: 'http://your.lan.or.internet.address:8080',

  initiativesUpdateInterval: 5, //Minutes in place to query for near initiative
  metersToConsiderInPlace: 150, //Person within this ammount of meters to trigger proximity notification
  minimumIntervalBetweenNotifications: 10 //Minutes


};
