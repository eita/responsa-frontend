//for now only one file

angular.module('filters')

    .filter('distance_km', function() {
        return function(input) {
            input = input || '';
            if (!isNaN(parseFloat(input))) { //if is numeric
                return parseFloat(input).toFixed(1).replace(/\.0/,'').replace('.',',') + ' km';
            } else {
                return ""
            }
        };
    });
