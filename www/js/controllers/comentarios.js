angular.module('controllers')
    .controller('ComentariosController',function($scope, Comentario, $ionicScrollDelegate) {
        $scope.teste = 'blabla';

        var comentavel = $scope.$parent.comentavel;

        $scope.comentarios = [];
        $scope.criarNovoComentario = function() {
            $scope.novoComentario = new Comentario({novo:true, texto:'', comentavel_id: comentavel.id, comentavel_type: comentavel.classType});
        };

        //Comentavel_type: precisa saber qual a classe
        var loadComentarios = function() {
            comentavel.loadComentarios().then(function(comentarios) {
                $scope.comentarios = comentarios;
                $scope.comentarios.push($scope.novoComentario);
            })
        };

        if ($scope.$parent.comentavel.$promise) {
            $scope.$parent.comentavel.$promise.then(function() {
                $scope.criarNovoComentario();
                loadComentarios();
            });
        } else {
            $scope.criarNovoComentario();
            loadComentarios();
        }

        $scope.postComment = function() {
            $scope.novoComentario.$save(function() {
                $scope.criarNovoComentario();
                $scope.comentarios.push($scope.novoComentario);
                $ionicScrollDelegate.scrollBottom(true);
            });
        }

    });