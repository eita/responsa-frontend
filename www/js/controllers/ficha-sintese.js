angular.module('controllers')


    .controller('FichaSinteseController',function($scope, $stateParams, ResponsaObject, Comentario){
        $scope.map_markers = [];
        $scope.ro_location = {};

        $scope.map_config_fs = {
            tileLayer: "http://{s}.tile.osm.org/{z}/{x}/{y}.png",
            maxZoom: 18,
            zoomControl: true,
            dragging: true,
            attributionControl: false
        };

        $scope.responsa_obj = ResponsaObject.get({id: $stateParams.id}, function(responsa_obj) {
            var marker = responsa_obj.marker();
            if (marker) {
                $scope.map_markers.push(marker);
                $scope.ro_location.lat = marker.lat;
                $scope.ro_location.lng = marker.lng;
                $scope.ro_location.zoom = 16;
            }
        });


        //Tela dos comentários
        $scope.comentavel = $scope.responsa_obj;

    });