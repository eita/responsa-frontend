angular.module('controllers')

    .controller('ResponsaController', function($scope,$rootScope,Forms, $cordovaBackgroundGeolocation, $window, BackgroundLocation) {

        $rootScope.settings = window.responsa.settings;

        //map configuration
        $rootScope.map_config = {
            tileLayer: "http://{s}.tile.osm.org/{z}/{x}/{y}.png",
            maxZoom: 15,
            attributionControl: false
        };

        //-------------------------------//
        //position updated by the user
        $rootScope.currentPosition = {};

        $rootScope._updateCurrentPositionTrial = 0;
        $rootScope.updateCurrentPosition = function() {
            console.log('Location: trying to update position, trial ' + ($rootScope._updateCurrentPositionTrial + 1));

            var posOptions = {
                enableHighAccuracy: true,
                timeout: 20000, // too long??
                maximumAge: 0
            };

            var posSuccess = function(position) {
                $rootScope.currentPosition = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                    zoom: 13
                };

                $rootScope.$broadcast('positionChanged');
                $rootScope._updateCurrentPositionTrial = 0;
                console.log('Location found: {' + position.coords.latitude.toString() + ',' + position.coords.longitude.toString() + '}')
            };

            var posError = function(error) {
                console.log('Location error: ' + JSON.stringify(error));

                $rootScope._updateCurrentPositionTrial++;

                if ($rootScope._updateCurrentPositionTrial == 3) {
                    //Not found: inform user or try alternative method, asking city or another type
                } else {
                    $rootScope.updateCurrentPosition();
                }

                //Try to find via server

                //Brasil inteiro
                //$scope.map_perto.setView([-14.48,-60.29],4);

                //Rio de Janeiro
                //$scope.map_perto.setView([-22.9873355,-43.4608085],13);

                //$scope.responsa_objects = ResponsaObject.query();
            };

            if (responsa.settings.testLocation) {
                setTimeout(function() {
                    posSuccess({
                        coords: responsa.settings.testLocation
                    });
                }, responsa.settings.testLocationTimeout || 0);

            } else {
                // cordova or html5 (posOptions is ignored)
                navigator.geolocation.getCurrentPosition(posSuccess, posError, posOptions);
            }

        };

        $rootScope.updateCurrentPosition();

        //-------------------------------//
        //position updated in background
        $rootScope.bgCurrentPositionTime = 0;
        $rootScope.bgCurrentPosition = {};
        $rootScope.listOfPositions = [];

        //-------------------------------//


        //Background geolocation

        BackgroundLocation.startMonitor();

        $scope.searchROs = function() {
            Forms.responsa_object.search($scope)
        };

    });
