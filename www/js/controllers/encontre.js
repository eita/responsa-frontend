function printIonicHistory() {
    if (ioh) {
        var hstr = "Histories:";
        hist = ioh.viewHistory().histories;
        var has_histories = false;
        for (var i in hist) {
            has_histories = true;
            hstr += "\n    " + i + ' (length=' + hist[i].stack.length;
            if (hist[i].parentHistoryId) {
                hstr += ', parent=' + hist[i].parentHistoryId;
            }
            hstr += "):";
            for (var j in hist[i].stack) {
                hstr += "\n        " + hist[i].stack[j].stateId;
            }
        }
        if (!has_histories) {
            hstr += ' none.'
        }
        console.log(hstr);
    }
}

angular.module('controllers')


    .controller('EncontreController',function($scope,$ionicHistory,$ionicNavBarDelegate, Forms, $state, $rootScope){
        window.ioh = $ionicHistory;

        var sc = $scope;

        $scope.position_known = false;

        $scope.$on('positionChanged', function() {
            if ($rootScope.currentPosition != {}) {
                sc.position_known = true;
            }
        });


        $scope.title = ' ';

        var tabTitles = [
            'Perto de você',
            'Favoritos',
            'Encontros'
        ];

        $scope.currentTab = 1;

        $scope.selectTab = function(tabNumber) {
            $scope.currentTab = tabNumber;
            $scope.title = tabTitles[$scope.currentTab-1];
            setTimeout(function() {
                $scope.$broadcast('selectTab'+tabNumber)
            },100);
        };

        $scope.$on('$ionicView.enter',function() {
            $scope.selectTab($scope.currentTab);
            console.log('entrou ' + new Date().toString());
        });


        $scope.novoLocalIniciativa = function() {
            //alert('a');
            Forms.local.new($scope);
        };

        $scope.handleClickResultado = function(responsa_object_id) {
            $state.go('app.ficha_sintese', {id: responsa_object_id});
        };

        /*
        Classes for tabs (example):
        [IOS] class="tabs-icon-only tabs-color-active-balanced tabs-bottom tabs-standard"
        [ANDROID] class="tabs-icon-only tabs-color-active-balanced tabs-top tabs-striped"

         */
        $ionicHistory.clearHistory();

    })

    .controller('EncontrePertoController',function($scope,ResponsaObject,$rootScope, $state, leafletData, $ionicLoading) {

        /*
        $ionicLoading.show({
            template: 'Obtendo tua localização...',
            hideOnStateChange: true,
            scope: $scope
        });
        */

        //Map configuration
        $scope.map_config_perto = {
            tileLayer: "http://{s}.tile.osm.org/{z}/{x}/{y}.png",
            maxZoom: 15,
            zoomControl: false,
            dragging: true,
            attributionControl: false
        };


        $scope.responsa_objects = [];
        $scope.map_markers = [];
        $scope.results_arrived = false;


        $scope.queryObjects = function() {
            if ($rootScope.currentPosition.lat) {
                console.log('queried objects');
                $scope.responsa_objects = ResponsaObject.query($rootScope.currentPosition, function (results) {
                    $scope.results_arrived = true;

                    var markers = [];

                    var ro_marker;
                    for (var i = 0 ; i < $scope.responsa_objects.length ; i++) {
                        ro_marker = $scope.responsa_objects[i].marker();
                        if (ro_marker) {
                            markers.push(ro_marker);
                        }
                    }

                    $scope.map_markers = markers;



                    leafletData.getMap('perto').then(function(map) {
                        var map_latlngs = [];
                        for (var i=0; i< markers.length; i++) {
                           map_latlngs.push([markers[i].lat,markers[i].lng]) ;
                        }
                        if (map_latlngs.length > 0) {
                            map.fitBounds(map_latlngs);
                        }
                    });

                });

            }

        };

        $scope.$on('positionChanged',function() {
            $scope.queryObjects();
            $scope.$broadcast('scroll.refreshComplete');
        });
        $scope.queryObjects();

        $scope.doRefresh = function() {
            $rootScope.updateCurrentPosition();
        };

        setTimeout(function() {
            var container = angular.element(document.getElementById('resultados'));
            container.on('scroll', function() {
                console.log('Container scrolled to ', container.scrollLeft(), container.scrollTop(), container.scrollTop() - container.height);
            });

        },100);

    })

    .controller('EncontreFavoritosController',function($scope, ResponsaObject, $rootScope) {
        $scope.$on('selectTab2',function() {
            console.log('2');
            if (!$rootScope.favoritos_updated) {
                $scope.results_arrived = false;
                $scope.responsa_objects = ResponsaObject.query({favoritos: true}, function(results) {
                    $scope.results_arrived = true;
                    $rootScope.favoritos_updated = true;
                });
            }
        });
    })

    .controller('EncontreEncontrosController',function($scope, ResponsaObject, Forms) {

        $scope.responsa_objects = ResponsaObject.query({encontros: true});

        $scope.novoEncontro = function() {
            Forms.encontro.new($scope);
        }
    })

    .controller('EncontrePesquisarController',function($scope, ResponsaObject) {
        $scope.responsa_objects = [];
        $scope.search_params = {
            palavra_chave: null
        };

        $scope.handleClickResultado = function(responsa_object_id) {
            $state.go('app.ficha_sintese', {id: responsa_object_id});
        };


        $scope.pesquisar = function() {
            $scope.responsa_objects = ResponsaObject.query({palavra_chave : $scope.search_params.palavra_chave}, function(results) {
                $scope.results_arrived = true;
            });
        };
    })

    .controller('EncontreResultados',function($scope, $state) {
        $scope.openFichaSintese = function(responsa_obj) {
            $state.go('app.ficha_sintese',{id: responsa_obj.id, obj: responsa_obj})
            $scope.$emit('ro_result_selected')
        };
    })
;