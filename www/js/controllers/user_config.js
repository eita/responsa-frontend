angular.module('controllers')

.controller('UserConfigController', function($scope, $rootScope, $upload, User, CameraFactory) {
  $rootScope.cachedUser = $scope.user = User.get({id: $rootScope.cachedUser.id});

  var updateSuccess = function(user) {
    history.back();
  }

  $scope.save = function() {
    this.user.$update(updateSuccess);
  }

  $scope.upload = function (data, contentType, originalFilename) {
    this.user.image_data = data
    this.user.image_content_type = contentType
    this.user.image_original_filename = originalFilename
    this.user.$update(updateSuccess);
  }

  $scope.selectAndUpload = function () {
    var options = {
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      // fix problem with android 4.4+
      // http://stackoverflow.com/questions/20638932/unable-to-load-image-when-selected-from-the-gallery-on-android-4-4-kitkat-usin
      destinationType: Camera.DestinationType.DATA_URL,
    }
    CameraFactory.getPicture(options).then(function (base64Data) {
      // FIXME: do not hardcode this
      var contentType = 'image/jpeg'
      $scope.upload(base64Data, contentType, 'userImage.jpg')
    }, function (err) {
      console.err(err)
    });

  }
});
