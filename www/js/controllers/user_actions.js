angular.module('controllers')
    .controller('ChegueiController', function($scope, $rootScope, ResponsaObject) {

        $scope.responsa_objects = [];
        $scope.results_arrived = false;

        if ($rootScope.currentPosition.lat) {
            console.log('cheguei controller - queried objects');
            $scope.responsa_objects = ResponsaObject.query($rootScope.currentPosition, function (results) {
                $scope.results_arrived = true;
            });
        }

        $scope.handleClickResultado = function() {
            alert('b');
        };

    });