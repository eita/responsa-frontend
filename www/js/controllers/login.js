angular.module('controllers')


    .controller('LoginController',function($scope, $ionicPopup, $state, $auth){
        // Form data for the login modal
        $scope.data = {};

        //Page states: NEW_USER / LOGIN / RECOVER_PASSWORD

        $scope.pageState = null;

        var alertErrorArray = function(error_array) {
            var error_message = "";
            for (var j=0; j < error_array.length; j++) {
                if (error_message != "") { error_message += '<br/>'}
                error_message += error_array[j]
            }
            var alertPopup = $ionicPopup.alert({
                template: error_message
            });
        };

        var alertErrorHash = function(error_hash) {
            var error_message = "";
            if (error_hash) {
                for (var i in error_hash) {
                    if (i == 'email' || i == 'password' || i == 'password_confirmation') {
                        for (var j=0; j < error_hash[i].length; j++) {
                            if (error_message != "") { error_message += '<br/>'}
                            error_message += error_hash[i][j]
                        }
                    }
                }
                var alertPopup = $ionicPopup.alert({
                    template: error_message
                });
            }
        };

        var doLogin = function() {
            window.a = $auth;
            $auth.submitLogin($scope.data)
                .then(function(resp) {
                //    $state.go('app.encontre');
                })
                .catch(function(resp) {
                    alertErrorArray(resp.errors);
                });
        };

        var doRecoverPassword = function() {
            $auth.requestPasswordReset($scope.data)
                .then(function(resp) {
                    var alertPopup = $ionicPopup.alert({
                        template: 'Foi enviado para o seu e-mail um link para definir uma nova senha.'
                    });
                    $scope.changeState('LOGIN');
                })
                .catch(function(resp) {
                    alertErrorArray(resp.data.errors);
                });
        };

        var doCreateUser = function() {
            $auth.submitRegistration($scope.data)
                .then(function(resp) { //Success
                    var alertPopup = $ionicPopup.alert({
                        title: 'Seu cadastro foi feito!',
                        template: 'Ainda falta completar seu cadastro, basta agora confirmar seu endereço de e-mail. Para isto, veja se recebeu uma mensagem na caixa postal e siga o link de confirmação.'
                    });
                    $scope.data.password = "";
                    $scope.data.password_confirmation = "";
                    $scope.changeState('LOGIN');
                })
                .catch(function(resp) { //Error
                    alertErrorHash(resp.data.errors);
                })
        };

        $scope.changeState = function(state) {
            switch (state) {
                case 'NEW_USER':
                    $scope.pageState = state;
                    $scope.pageTitle = 'Cadastre-se';
                    $scope.buttonTitle = 'Completar cadastro';
                    break;

                case 'LOGIN':
                    $scope.pageState = state;
                    $scope.pageTitle = 'Login';
                    $scope.buttonTitle = 'Entrar';
                    break;

                case 'RECOVER_PASSWORD':
                    $scope.pageState = state;
                    $scope.pageTitle = 'Recuperar senha';
                    $scope.buttonTitle = 'Recuperar senha';
                    break;

            }
        };

        $scope.changeState('LOGIN');

        //Action done when the button is clicked
        $scope.doAction = function() {
            switch($scope.pageState) {
                case 'NEW_USER':
                    doCreateUser();
                    break;
                case 'LOGIN':
                    doLogin();
                    break;
                case 'RECOVER_PASSWORD':
                    doRecoverPassword();
                    break;
            }
        };

    });

