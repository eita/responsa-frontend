angular.module('controllers')

.controller('CameraTestController', function($scope, CameraFactory) {

  $scope.getPhoto = function() {
    var options = {
      quality: 75,
      saveToPhotoAlbum: false,
    };
    CameraFactory.getPicture(options).then(function (imageURI) {
      $scope.imageURI = imageURI;
      $scope.lastPhoto = imageURI;
    }, function (err) {
      console.err(err);
    });
  };
  $scope.getPhotoFile = function() {
    var options = {
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      // fix problem with android 4.4+
      // http://stackoverflow.com/questions/20638932/unable-to-load-image-when-selected-from-the-gallery-on-android-4-4-kitkat-usin
      destinationType: Camera.DestinationType.DATA_URL,
    }
    CameraFactory.getPicture(options).then(function (base64Data) {
      $scope.lastPhoto = "data:image/jpeg;base64," + base64Data;

    }, function (err) {
      console.err(err);
    });
  }
})

.controller('GeolocationTestController',function($scope) {

  $scope.getPositionLow = function() {
    navigator.geolocation.getCurrentPosition(function(position){
      $scope.coords = position.coords;
    })
  };

  $scope.getPositionHigh = function() {
    navigator.geolocation.getCurrentPosition(function(position){
      $scope.coords2 = position.coords;
    })
  };
})

.controller('MainTestController',function($scope,$ionicPopover, $ionicActionSheet, $ionicModal, $cordovaLocalNotification) {
  //popover test
  var popoverTemplate = '<ion-popover-view><ion-header-bar> <h1 class="title">My Popover Title</h1> </ion-header-bar> <ion-content> Hello! </ion-content></ion-popover-view>';


  $scope.popover = $ionicPopover.fromTemplate(popoverTemplate, {
    scope: $scope
  });

  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });


  //--------------------------------------------------
  //Action sheet test
  $scope.actionSheet = function() {
    var hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: '<b>Share</b> This' },
        { text: 'Move' }
      ],
      destructiveText: 'Delete',
      titleText: 'Modify your album',
      cancelText: 'Cancel',
      cancel: function() {
        // add cancel code..
      },
      buttonClicked: function(index) {
        return true;
      }
    });
  }

  //--------------------------------------------------
  //Modal test
  $ionicModal.fromTemplateUrl('templates/tests/modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  //Local Notification
  $scope.localNotification = function() {
    $cordovaLocalNotification.add({
      id: 1,
      title: 'Title here',
      text: 'Text here',
      data: {
        customProperty: 'custom value'
      }
    }).then(function (result) {
      alert('a')
    });
  };

})

;
