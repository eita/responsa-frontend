Responsa - app do Portal do Consumo Responsável
===============================================

## Node and NPM install

    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

## Install

    cd responsa-frontend-git-path
    sudo npm install -g gulp ionic cordova
    npm install

## Android Build

    sudo apt-get install default-jdk ant

### Android SDK tools install

    mkdir ~/android && cd ~/android
    wget http://dl.google.com/android/android-sdk_r24.1.2-linux.tgz
    tar xzf android-sdk_r24.1.2-linux.tgz
    echo "export ANDROID_HOME=~/android/android-sdk-linux" >> ~/.bashrc
    echo "export PATH+=:~/$ANDROID_HOME/tools:~/$ANDROID_HOME/platform-tools" >> ~/.bashrc

Then start bash (terminal) again to 

### Android emulator

Run `android` and then install "SDK Platform", "Intel x86 Atom System Image" and "Google APIs" from the version you want to emulate (e.g. 4.0, 4.1, 5.1)

Then run `android avd` and configure your system. Then start it.

### Ionic
  
    cordova platform add android
    ionic build android
    ionic emulate android

