#!/bin/sh
rm CordovaApp.apk
ionic build --release android
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore platforms/android/ant-build/CordovaApp-release-unsigned.apk responsa
zipalign -v 4 platforms/android/ant-build/CordovaApp-release-unsigned.apk CordovaApp.apk

