var gulp = require('gulp');
var spawn = require('child_process').spawn;
var gutil = require('gulp-util');
var bower = require('bower');

var concat = require('gulp-concat');
var rename = require('gulp-rename');
var sh = require('shelljs');

var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');

var babel = require('gulp-babel');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('default', ['gulp-reload', 'sass', 'babel']);

var paths = {
  sass: ['./scss/**/*.scss'],
  es6: ['www/**/*.es6'],
};
gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.es6, ['babel']);
  gulp.watch('gulpfile.js', ['gulp-reload']);
});

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('babel', function() {
  return gulp.src(paths.es6)
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(sourcemaps.write('.'))
    .pipe(rename({ extname: '.js' }))
    .pipe(gulp.dest('www'))
});

gulp.task('gulp-reload', function() {
  spawn('gulp', ['watch'], {stdio: 'inherit'});
  process.exit();
});

gulp.task('install', function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

