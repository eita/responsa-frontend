ANT_HOME=/usr/local/apache-ant-1.9.4
ANT_OPTS="-Xmx256M"
ANDROID_HOME=/home/android/android-sdk-linux
ANDROID_SDK_ROOT=/home/android/android-sdk-linux

PATH=$PATH:$HOME/.rvm/bin:$ANT_HOME/bin:$ANDROID_HOME/tools # Add RVM to PATH for scripting

